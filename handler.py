# from __future__ import print_function

import base64
from distutils.log import debug
import json
from urllib import response
import boto3
import os
import time
import csv 
import sys

# from xml.etree.ElementTree import XML, fromstring
import xml.etree.ElementTree as ET

print('Loading function')

cliente = boto3.client('s3')
maxdepth = 0

def hello(event, context):
    
    for record in event['Records']:
        response = []
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']
        obj = cliente.get_object(Bucket=bucket, Key=key)
        file_content = obj['Body'].read()
        root = ET.fromstring(file_content)
        # depth(root, -1)
        # print(maxdepth)

        # maxdepth = 0
        for child_1 in root:
            keyword = buscar_keywords(child_1.tag, child_1.attrib, root)
            if keyword != 0:
                response.append(keyword)
                # print("=== ENCONTRADO  =====")


            for child_2 in child_1:
                keyword = buscar_keywords(child_2.tag, child_2.attrib, child_1)
                if keyword != 0:
                    response.append(keyword)
                    # print("=== ENCONTRADO  =====")

                for child_3 in child_2:
                    keyword = buscar_keywords(child_3.tag, child_3.attrib, child_2)
                    if keyword != 0:
                        response.append(keyword)
                        # print("=== ENCONTRADO  =====")


                    for child_4 in child_3:
                        keyword = buscar_keywords(child_4.tag, child_4.attrib, child_3)
                        if keyword != 0:
                            response.append(keyword)
                            # print("=== ENCONTRADO  =====")


                        for child_5 in child_4:
                            keyword = buscar_keywords(child_5.tag, child_5.attrib, child_4)
                            if keyword != 0:
                                response.append(keyword)
                                # print("=== ENCONTRADO  =====")


                            for child_6 in child_5:
                                keyword = buscar_keywords(child_6.tag, child_6.attrib, child_5)
                                if keyword != 0:
                                    response.append(keyword)
                                    # print("=== ENCONTRADO  =====")


                                for child_7 in child_6:
                                    keyword = buscar_keywords(child_7.tag, child_7.attrib, child_6)
                                    if keyword != 0:
                                        response.append(keyword) 
                                        # print("=== ENCONTRADO  =====")

        response = agregar_keyword_estatica(response)

    codigo = obtener_config_tipo_dte(response, "xml")
    print(codigo)
    return response
    # return maxdepth

def depth(elem, level):
    global maxdepth
    # your code goes here
    # XML2 - Find the Maximum Depth in Python - Hacker Rank Solution START
    if (level == maxdepth):
        maxdepth += 1
    for child in elem:
        depth(child, level + 1)
    # XML2 - Find the Maximum Depth in Python - Hacker Rank Solution END

def buscar_keywords(tag, attrib, nodos):
    # print(attrib)
    respuesta = 0
    if tag == "TipoDTE":
        respuesta = {
            "id": 1592,
            "valor": nodos.find("TipoDTE").text
        }        
        # print("=== ENCONTRADO TIPODTE =====")

    if tag == "RUTEmisor":
        respuesta = {
            "id": 1590,
            "valor": nodos.find("RUTEmisor").text
        }  
        # print("=== ENCONTRADO RUTEMISOR =====")

    if tag == "RznSoc":
        respuesta = {
            "id": 1591,
            "valor": nodos.find("RznSoc").text
        }  
        # print("=== ENCONTRADO RZNSOC =====")

    if tag == "Folio":
        respuesta = {
            "id": 1593,
            "valor": nodos.find("Folio").text
        }  

    if tag == "FchEmis":
        respuesta = {
            "id": 1594,
            "valor": nodos.find("FchEmis").text
        }  
    
    if tag == "RUTRecep":
        respuesta = {
            "id": 1597,
            "valor": nodos.find("RUTRecep").text
        }  

    if tag == "RznSocRecep":
        respuesta = {
            "id": 1598,
            "valor": nodos.find("RznSocRecep").text
        } 

    if tag == "CmnaRecep":
        respuesta = {
            "id": 1599,
            "valor": nodos.find("CmnaRecep").text
        }  

    if tag == "CiudadRecep":
        respuesta = {
            "id": 1600,
            "valor": nodos.find("CiudadRecep").text
        }  

    if tag == "MntTotal":
        respuesta = {
            "id": 1601,
            "valor": nodos.find("MntTotal").text
        }  

    if tag == "RUT_INTERMEDIARIO":
        respuesta = {
            "id": 1604,
            "valor": nodos.find("RUT_INTERMEDIARIO").text
        }  

    if tag == "SUCURSAL_BICEVIDA":
        respuesta = {
            "id": 152,
            "valor": nodos.find("SUCURSAL_BICEVIDA").text
        }  

    if tag == "SUC_CLIENTE":
        respuesta = {
            "id": 1605,
            "valor": nodos.find("SUC_CLIENTE").text
        }  

    return respuesta

def agregar_keyword_estatica(keyword):
    response = keyword
    venta_keyword = {
        "id" : 936,
        "valor": "VENTA"
    }
    response.append(venta_keyword)
    return response

def obtener_config_tipo_dte(body, tipo_doc):
    #buscar tag dte factura, factura exenta, nota de credito, nota de debito
    response = 0
    tipo_dte = -1
    for e in body:
        if e['id'] == 1592:
            tipo_dte = e['valor']
            print("========= DTE ENCONTRADO EN BODY ==========")
            print(tipo_dte)

    if tipo_dte == -1 :
        print("=====NUMERO DTE NO ENCONTRADO EN BODY KEYWORDS=====")
    
    response = obtener_codigo(tipo_dte, tipo_doc)
    return response

def obtener_codigo(tipo_dte, tipo_doc):
    response = 0
    codigos_por_tipo_doc = [
        #TIPO AFECTA
        {
            "id": 33,
            "pdf": 551,
            "xml": 555 
        },
        #TIPO EXENTA
        {
            "id": 34,
            "pdf": 552,
            "xml": 556
        },
        #TIPO NOTA DEBITO
        {
            "id": 56,
            "pdf": 554,
            "xml": 558
        },
        #TIPO NOTA CREDITO
        {
            "id": 61,
            "pdf": 553,
            "xml": 557
        }
    ]
    for e in codigos_por_tipo_doc:
        # print(e['id'])
        if e['id'] == int(tipo_dte):
            response = e[tipo_doc]
            print("CODIGO XML O PDF ENCONTRADO")
    return response


